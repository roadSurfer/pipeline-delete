#!/usr/bin/env bash

set -eE -o pipefail -o noclobber -o nounset
shopt -s lastpipe

# Traps are not inherited by subshells, so we have to make sure each one picks this up
# This isn't strictly needed, but it allows for future enhancement and already gives a basic stacktrace
# We cannot us any other values/functions as we cannot be sure where the failure happened
# We increment this so we know how many times this file has been sourced, which gives us an approximate stack depth
export STACK_DEPTH=$((${STACK_DEPTH:-0}+1))
function handle_error() {
  local location=
  local i=0
  while read -r line func file; do
    if [[ "${file}" == "environment" ]]; then
      location="a shared function"
    else
      location="${file}:${line}"
    fi
    if [[ "${func}" != "main" ]]; then
      location="${location}, ${func}()"
    fi
    if [[ $(type -t log) = function ]]; then
      log 1 "Error in ${location}"
    else
      echo "Error in ${location}"
    fi
  done < <(while caller $i; do (( i=i+1 )); done)

  if [[ ${STACK_DEPTH} -eq 1 ]]; then
    if [[ $(type -t notify) = function ]]; then
      notify 1 "Script failed, check output" "${use_toast:-Y}"
    else
      echo "Script failed, check output"
      printf '\a'
    fi
  fi
}
# And this makes sure we get trapped
trap 'handle_error' ERR

accessToken=
apiUrl=
fullApiUrl=

pipelinesToKeep=20
pageSize=10

groupDive=N
groupId=
groupName=
groupSet=

projectId=
projectName=
projectSet=

toRetain="repository/tags"
retainLabel="Tagged"

# The logging level - if set to 0, nothing will be logged to the terminal
export LOG_LEVEL=${LOG_LEVEL:-3}

# Colours etc used for display and general awesomeness
export RED='\033[1;31m'
export DARK_GREEN='\033[0;32m'
export GREEN='\033[1;32m'
export BLUE='\033[1;34m'
export YELLOW='\033[1;33m'
export ORANGE='\033[0;33m'
export NC='\033[0m' # No Color, reset back to normal
if [[ $TERM != "dumb" ]]; then
  # tput is only happy on an proper terminal
  BOLD=$(tput bold)
  UNDERLINE=$(tput smul)
  OFFUNDERLINE=$(tput rmul)
  NORMAL=$(tput sgr0)
else
  BOLD=
  UNDERLINE=
  OFFUNDERLINE=
  NORMAL=
fi
export BOLD
export UNDERLINE
export OFFUNDERLINE
export NORMAL

function usage() {
	echo "Usage: $0 [other options] -a PAT -s URL"
	echo "       Deletes older pipelines from GitLab. A group or project must be supplied."
	echo "       Examples:"
	echo "           $0 -gFoo%20Bar -k20 -a glpat-abcde-1234 -s https://my.gitlab.server/"
	echo "           $0 -vv -pFoo -a glpat-abcde-1234 -s https://my.gitlab.server/"
	echo "           $0 -vv -p456 -b \"\" -a glpat-abcde-1234 -s https://my.gitlab.server/"
	echo
	echo "Options:"
	echo "       -a"
	echo "              ${BOLD}(Mandatory)${NORMAL} Your personal access token"
	echo "       -d"
	echo "              If processing groups, dive into sub-groups recursively"
	echo "       -g"
	echo "              The group id to process, or the name in a URL safe format. e.g 'Foo%20Bar'"
	echo "              Partial names are accepted and all matches will be iterated over"
	echo "       -h, --help"
	echo "              This information."
	echo "       -k"
	echo "              (Default: ${pipelinesToKeep}) The number of pipelines to keep"
	echo "       -p"
	echo "              The project id to process, or the name in a URL safe format. e.g 'Foo%20Bar'"
	echo "              Partial names are accepted and all matches will be iterated over"
	echo "              If group name or id has been specificed, this value ${BOLD}must be${NORMAL} a name for filtering projects within the group"
	echo "       -r"
	echo "              Retain pipelines related to only Releases and not all Tags"
	echo "       -s"
	echo "              ${BOLD}(Mandatory)${NORMAL} The server URL"
	echo "       -v, -vv"
	echo "              Displays additional information when running. 'vv' increases logging even further"
}

# Echos a message if it is appropriate to the log level
# Param 1 - The intended logging level
# Values:
#   1 - An error
#   2 - A warning
#   3 - Information
#   4 - Debug
#   5 - Trace
# Param 2 - The message to display
function log() {
  local message=${*:2}
	if [[ "$1" -le ${LOG_LEVEL} ]]; then
		case "$1" in
			1)
				echo -e "[${RED}ERROR${NC}  ] ${message}"
				;;
			2)
				echo -e "[${ORANGE}WARN${NC}   ] ${message}"
				;;
			3)
				echo -e "[${BLUE}INFO${NC}   ] ${message}"
				;;
			4)
				echo -e "[${GREEN}DEBUG${NC}  ] ${message}"
				;;
			5)
				echo -e "[${DARK_GREEN}TRACE${NC}  ] ${message}"
				;;
			*)
				echo "Unknown logging option '$1', message '${message}'"
				exit 3
				;;
		esac
	fi
}

# 1 - Message
# 2 - The response
function badResponse() {
	local headers=$(echo "$2" | sed -n '/^$/q;p')
	local json=$(echo "$2" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
	local responseCode=$(echo "$2" | tail -n1)

	log 1 "$1"
	log 1 "Response code: ${responseCode}"
	log 1 "Headers:\n${headers}"
	log 1 "JSON:\n${json}"
	exit 1
}

# 1 - Group JSON
# 2 - Whether or not to dive into groups
# 3 - (Optional) A project name to filter on
function doGroup() {
	local groupId=$(echo "$1" | jq -r '.id' )
	local groupName=$(echo "$1" | jq -r '.name')
	log 3 "Processing group '${groupName}' (${groupId})"

	if [[ "$2" == "Y" ]]; then
		local groupP=1
		local response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${apiUrl}/groups/${groupId}/subgroups?per_page=${pageSize}&page=${groupP}" | sed 's/\r//g' )
		local responseCode=$(echo "${response}" | tail -n1)

		if [[ ${responseCode} -ne 200 ]]; then
			badResponse "Group not found or some other issue encountered" "${response}"
		fi
		local headers=$(echo "$response" | sed -n '/^$/q;p')
		local json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
		local totalSubGroups=$(echo "${headers}" | grep -Fi 'x-total:' | cut -d' ' -f2)

		if [[ ${totalSubGroups} -gt 0 ]]; then
			local totalPages=$(echo "${headers}" | grep -Fi 'x-total-pages:' | cut -d' ' -f2 )
			log 4 "Group '${groupName}' (${groupId}) has ${totalSubGroups} sub-groups in ${totalPages} pages"

			until [ ${groupP} -gt ${totalPages} ]; do
				log 5 "Processing subgroup page ${groupP} of ${totalPages} pages"
				echo "${json}" | jq -cr '.[]' | while read -r line; do
					doGroup "$line" "$2" "$3"
				done
				((groupP++))
				response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${apiUrl}/groups/${groupId}/subgroups?per_page=${pageSize}&page=${groupP}" | sed 's/\r//g' )
				responseCode=$(echo "${response}" | tail -n1)

				if [[ ${responseCode} -ne 200 ]]; then
					badResponse "Group not found or some other issue encountered" "${response}"
				fi
				headers=$(echo "$response" | sed -n '/^$/q;p')
				json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
			done
		else
			log 4 "Group '${groupName}' (${groupId}) has no sub-groups"
		fi
	fi

	projectP=1
	local projectApiUrl="${apiUrl}/groups/${groupId}/projects?"
	if [[ -n $3 ]]; then
		projectApiUrl="${projectApiUrl}search=$3&"
	fi
	response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${projectApiUrl}per_page=${pageSize}&page=${projectP}" | sed 's/\r//g' )
	responseCode=$(echo "${response}" | tail -n1)

	if [[ ${responseCode} -ne 200 ]]; then
		badResponse "Group not found or some other issue encountered" "${response}"
	fi
	headers=$(echo "$response" | sed -n '/^$/q;p')
	json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
	local totalProjects=$(echo "${headers}" | grep -Fi 'x-total:' | cut -d' ' -f2)

	if [[ ${totalProjects} -gt 0 ]]; then
		local totalPages=$(echo "${headers}" | grep -Fi 'x-total-pages:' | cut -d' ' -f2 )
		log 4 "Group '${groupName}' (${groupId}) has ${totalProjects} projects in ${totalPages} pages"

		until [ ${projectP} -gt ${totalPages} ]; do
			log 5 "Processing project page ${projectP} of ${totalPages} pages"
			echo "${json}" | jq -cr '.[]' | while read -r line; do
				doProject "$line"
			done
			((projectP++))
			response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${projectApiUrl}per_page=${pageSize}&page=${projectP}" | sed 's/\r//g' )
			responseCode=$(echo "${response}" | tail -n1)

			if [[ ${responseCode} -ne 200 ]]; then
				badResponse "Group not found or some other issue encountered" "${response}"
			fi
			json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
		done
	else
		log 4 "Group '${groupName}' (${groupId}) has no projects"
	fi
}

# 1 - Project JSON
function doProject() {
	local projectId=$(echo "$1" | jq -r '.id' )
	local projectName=$(echo "$1" | jq -r '.name')
	log 3 "Checking project '${projectName}' (${projectId})"
	local buildsAccessLevel=$(echo "$1" | jq -r '.builds_access_level')
	local projectArchived=$(echo "$1" | jq -r .archived)

  if [[ "${buildsAccessLevel}" == "disabled" ]]; then
  	log 2 "*** *** *** ***"
  	log 2 "*** *** *** ***"
  	log 2 "*** *** *** ***"
  	log 2 "Project '${projectName}' (${projectId}) has CI/CD disabled"
  	log 2 "Project '${projectName}' (${projectId}) has CI/CD disabled"
  	log 2 "Project '${projectName}' (${projectId}) has CI/CD disabled"
  	log 2 "*** *** *** ***"
  	log 2 "*** *** *** ***"
  	log 2 "*** *** *** ***"
	elif [[ "${projectArchived}" == "true" ]]; then
		log 2 "Project '${projectName}' (${projectId}) is archived and pipelines cannot be deleted"
	else
		local retainJSON=$(curl -s --header "PRIVATE-TOKEN:${accessToken}" "${apiUrl}/projects/${projectId}/${toRetain}")
		local retainCommitIds=($(echo "${retainJSON}" | jq -r '.[].commit.id'))

		# The the first page, we only care about the headers
		local response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${apiUrl}/projects/${projectId}/pipelines?per_page=${pipelinesToKeep}&page=1" | sed 's/\r//g' )
		local responseCode=$(echo "${response}" | tail -n1)

		if [[ ${responseCode} -ne 200 ]]; then
			badResponse "Project not found or some other issue encountered" "${response}"
		fi
		local headers=$(echo "$response" | sed -n '/^$/q;p')
		local totalPipelines=$(echo "${headers}" | grep -Fi 'x-total:' | cut -d' ' -f2)

		if [[ ${totalPipelines} -gt ${pipelinesToKeep} ]]; then
			local totalPages=$(echo "${headers}" | grep -Fi 'x-total-pages:' | cut -d' ' -f2 )
		  log 4 "Project '${projectName}' (${projectId}) has ${totalPipelines} over ${totalPages} pages"

      local pipelineP=${totalPages}
			until [ ${pipelineP} -eq 2 ]; do
				response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${apiUrl}/projects/${projectId}/pipelines?per_page=${pipelinesToKeep}&page=${pipelineP}&order_by=id&sort=desc" | sed 's/\r//g' )
				responseCode=$(echo "${response}" | tail -n1)

				if [[ ${responseCode} -ne 200 ]]; then
					badResponse "Project not found or some other issue encountered" "${response}"
				fi
				json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
		    local pipelineIds=($(echo "${json}" | jq -r '.[].id'))
				log 4 "Processing ${#pipelineIds[@]} pipelines on page ${pipelineP} of ${totalPages} pages for project id '${projectName}' (${projectId})"

				for pipelineId in "${pipelineIds[@]}"; do
					local pipelineCommitId=$(echo "${json}" | jq -r --argjson pipeId "${pipelineId}" 'map(select(.id==$pipeId))[].sha')
					if [[ ${retainCommitIds[@]} =~ ${pipelineCommitId} ]]; then
						log 4 "Pipeline ${pipelineId} on page ${pipelineP} for project '${projectName}' (${projectId}) is a '${retainLabel}' pipeline, skipping"
					else
						log 5 "Deleting old pipeline ${pipelineId} from project id '${projectName}' (${projectId})"
						response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" -X DELETE "${apiUrl}/projects/${projectId}/pipelines/${pipelineId}" | sed 's/\r//g' )
						responseCode=$(echo "${response}" | tail -n1)

						if [[ ${responseCode} -ne 204 ]]; then
							badResponse "Failed to delete pipeline ${pipelineId} from '${projectName}' (${projectId})" "${response}"
						fi
					fi
				done
				((pipelineId--))
			done
		else
			log 4 "Project '${projectName}' (${projectId}) has no pipelines to process (${totalPipelines} is less than the retain of ${pipelinesToKeep})"
		fi
	fi
}

OPTIONS=a:dg:hk:p:rs:v
LONGOPTS=help

# Use ! and PIPESTATUS to get exit code with errexit set
#   - Temporarily store output to be able to check for errors
#   - Activate quoting/enhanced mode (e.g. by writing out “--options”)
#   - Pass arguments only via -- "$@" to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
	# Return value is non-zero, ergo getopt has complained about wrong arguments to stdout
	usage
	exit 2
fi

# Read getopt’s output this way to handle the quoting right:
eval set -- "${PARSED}"

while true; do
	case "$1" in
		-a)
			accessToken=$2
			shift
			;;
		-d)
			groupDive=Y
			;;
		-g)
			if [[ $2 =~ ^-?[0-9]+$ ]]; then
				groupId=$2
			else
				groupName=$2
			fi
			groupSet=Y
			shift
			;;
		-k)
			if [[ $2 =~ ^-?[0-9]+$ ]]; then
				pipelinesToKeep=$2
			else
				log 1 "Pipeline to keep '$2' does not appear to be an integer"
				exit 1
			fi
			shift
			;;
		-h | --help)
			usage
			exit
			;;
		-p)
			if [[ $2 =~ ^-?[0-9]+$ ]]; then
				projectId=$2
			else
				projectName=$2
			fi
			projectSet=Y
			shift
			;;
		-r)
			log 3 "${BOLD}Only${NORMAL} retaining 'Releases'"
			toRetain="releases"
			retainLabel="Release"
			;;
		-s)
			if [[ "${2: -1}" == "/" ]]; then
				apiUrl="$2api/v4"
			else
				apiUrl="$2/api/v4"
			fi
			shift
			;;
		-v)
			if [[ ${LOG_LEVEL} -gt 4 ]]; then
				log 2 "Cannot increase logging further"
			else
				((LOG_LEVEL++))
			fi
			;;
		--)
			break
			;;
		*)
			log 1 "Programming error in '$0', unknown option, '$1'"
			exit 3
			;;
	esac
	shift
done

if [[ -z ${accessToken} ]]; then
	log 1 "Personal Access Token not supplied, see help '$0 --help'"
	exit 1
fi

if [[ -z ${apiUrl} ]]; then
	log 1 "Server URL not supplied, see help '$0 --help'"
	exit 1
fi

response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${apiUrl}/personal_access_tokens/self" | sed 's/\r//g')
responseCode=$(echo "${response}" | tail -n1)

if [ "${responseCode}" -ne 200 ]; then
	badResponse "Unauthorized/Invalid - Check your private key" "${response}"
fi

if [[ -n ${groupName} ]]; then
	log 4 "Fetching group(s) with name '${groupName}'..."
	fullApiUrl="${apiUrl}/groups?with_projects=false&top_level_only=yes&search=${groupName}&per_page=${pageSize}&"
elif [[ -n ${groupId} ]]; then
	log 4 "Fetching group with id '${groupId}'"
	fullApiUrl="${apiUrl}/groups/${groupId}?with_projects=false&"
elif  [[ -n ${projectName} ]]; then
	log 4 "Fetching project(s)  with name '${project}'..."
	fullApiUrl="${apiUrl}/projects?search=${projectName}&per_page=${pageSize}&"
elif [[ -n ${projectId} ]]; then
	log 4 "Fetching project with id '${projectId}'"
	fullApiUrl="${apiUrl}/projects/${projectId}?"
else
	log 2 "This will process ${BOLD}ALL GROUPS${NORMAL}"
	read -rp "Are you sure? (y/N): " response
	if [[ $response == [yY] || $response == [yY][eE][sS] ]]; then
		groupSet="Y"
		fullApiUrl="${apiUrl}/groups?with_projects=false&top_level_only=yes&per_page=${pageSize}&"
	else
		log 3 "Quitting"
		exit
	fi
fi

mainP=1
response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${fullApiUrl}page=${mainP}" | sed 's/\r//g' )
responseCode=$(echo "${response}" | tail -n1)

if [[ ${responseCode} -ne 200 ]]; then
	badResponse "No groups/projects found or some other issue encountered" "${response}"
fi

headers=$(echo "$response" | sed -n '/^$/q;p')
json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
totalPages=$(echo "${headers}" | grep -Fi 'x-total-pages:' | cut -d' ' -f2 || true)
totalPages=${totalPages:-1}
numItems=$(echo "${headers}" | grep -Fi 'x-total:' | cut -d' ' -f2 || true)
numItems=${numItems:-0}

# If it's 0, it's probably a single item based on Id but let's be paranoid
if [[ numItems -eq 0 ]]; then
	if echo "${json}" | jq -e 'type == "array"' > /dev/null; then
			# Count the number of elements in the array
			numItems=$(echo "${json}" | jq 'length')
	else
			# It's a single object, so the count is 1
			numItems=1
			# Make the JSON an arry, it simplifies things later
			json="[${json}]"
	fi
fi

if [[ ${numItems} -eq 0 ]]; then
	badResponse "No groups/projects seem to be in response" "${response}"
fi

log 3 "${numItems} groups/projects found"
until [ ${mainP} -gt ${totalPages} ]; do
	log 3 "Processing page ${mainP} of ${totalPages} pages"
	echo "${json}" | jq -cr '.[]' | while read -r line; do
		if [[ -n ${groupSet} ]]; then
			doGroup "$line" "${groupDive}" "${projectName}"
		else
			doProject "$line"
		fi
	done
	((mainP++))
	response=$(curl -s -D - -w "\n%{http_code}\n" --header "PRIVATE-TOKEN:${accessToken}" "${fullApiUrl}page=${mainP}" | sed 's/\r//g' )
	responseCode=$(echo "${response}" | tail -n1)

	if [[ ${responseCode} -ne 200 ]]; then
		badResponse "No groups found or some other issue encountered" "${response}"
	fi

	json=$(echo "${response}" | sed -n '/^$/,$p' | tail -n +2 | head -n -1)
done

log 3 "Processing is complete"
