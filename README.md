# Deleting old GitLab Pipeplins

Written to address [GitLab issue 338480](https://gitlab.com/gitlab-org/gitlab/-/issues/338480#note_1498394655), the 
script [here](./src/bash/deletePipelines.sh) can reach into GitLab and delete old pipelines via the API. It can process one project,
or a group and optionally all sub-groups within that group. By default, it will attempt to delete all but the last 20
pipelines.

It relies on you having created a [Personal Access Token](https://www.gitlab.com/help/user/profile/personal_access_tokens.md#create-a-personal-access-token)
with `API` access.

Group and Projects names can be provided, these are treated as being wildcards for searches. Or you can specify 
specific id(s) to target.

Using it is simple, and help is available by running `./deletePipelines.sh -h`.

Say you have the project "Foo" (project id "123") and you want to delete all pipelines apart from the last 10, you 
would run one of these two commands:
* `./deletePipelines.sh -p Foo -k 10 -a gplat-abcde-1234 -s https://your.gitlab.server/`; or
* `./deletePipelines.sh -p 123 -k 10 -a gplat-abcde-1234 -s https://your.gitlab.server/`

If you only want to process projects that were direct children of "Bar" (group id "456"):
* `./deletePipelines.sh -g Bar -a gplat-abcde-1234 -s https://your.gitlab.server/`; or
* `./deletePipelines.sh -g 456 -a gplat-abcde-1234 -s https://your.gitlab.server/`

Or maybe you want to process group "Bar" and clean up all projects in all sub-groups:
* `./deletePipelines.sh -g Bar -d -a gplat-abcde-1234 -s https://your.gitlab.server/`; or
* `./deletePipelines.sh -g 456 -d -a gplat-abcde-1234 -s https://your.gitlab.server/`

And finally, perhaps you only want to delete old pipelines for projects with "Foo" in their name from group "Bar":
* `./deletePipelines.sh -g Bar -p Foo -d -a gplat-abcde-1234 -s https://your.gitlab.server/`; or
* `./deletePipelines.sh -g 456 -p Foo -d -a gplat-abcde-1234 -s https://your.gitlab.server/`

* The order of command switches does not matter.

## Options

| Option        | Purpose                                                                                                                                                                                                                                                                |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| -a            | (**Mandatory**) Your personal access token                                                                                                                                                                                                                             |
| -d            | If processing groups, dive into sub-groups recursively                                                                                                                                                                                                                 |
| -g            | The group id to process, or the name in a URL safe format. e.g `Foo%20Bar`<br/>Partial names are accepted and all matches will be iterated over                                                                                                                        |
| -h,<br>--help | Help information                                                                                                                                                                                                                                                       |
| -k            | (Default: 50) The number of pipelines to keep                                                                                                                                                                                                                          |
| -p            | The project id to process, or the name in a URL safe format. e.g `Foo%20Bar`"<br/>Partial names are accepted and all matches will be iterated over"<br/>If group name or id has been specified, this value **must be** a name for filtering projects within the group" |
| -r            | Retain pipelines related to only Releases and not all Tags                                                                                                                                                                                                             |
| -s            | (**Mandatory**) The server URL                                                                                                                                                                                                                                         |  
| -v,<br/>-vv   | Displays additional information when running. 'vv' increases logging even further                                                                                                                                                                                      |
